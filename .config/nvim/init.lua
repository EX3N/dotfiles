vim.api.nvim_command [[source ~/.config/nvim/modules/plugins.vim]]
vim.api.nvim_command [[source ~/.config/nvim/modules/general.vim]]
vim.api.nvim_command [[source ~/.config/nvim/modules/colors.vim]]
vim.api.nvim_command [[source ~/.config/nvim/modules/airline.vim]]
vim.api.nvim_command [[source ~/.config/nvim/modules/nerdtree.vim]]
vim.api.nvim_command [[source ~/.config/nvim/modules/fzf.vim]]
vim.api.nvim_command [[source ~/.config/nvim/modules/animation.vim]]

require'lspconfig'.clangd.setup{}
