filetype plugin indent on
"syntax on
set mouse=a
set background=dark
set mousehide
scriptencoding utf-8
set encoding=utf-8
set number
set relativenumber
set clipboard=unnamed,unnamedplus
set ignorecase
set smartcase
set list
set showmatch
set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
set hidden
set nobackup
set nowritebackup
set cmdheight=1
set cursorline

nnoremap <silent> <C-H> <C-W>h
nnoremap <silent> <C-J> <C-W>j
nnoremap <silent> <C-K> <C-W>k
nnoremap <silent> <C-L> <C-W>l
nnoremap <C-M> :Neoformat<CR>

nnoremap <silent> gh    :LspSagaFinder<CR>
nnoremap <silent> K     :LspSagaHoverDoc<CR>

au BufEnter *.h  let b:fswitchdst = "c,cpp,cc,m"
au BufEnter *.cc let b:fswitchdst = "h,hpp"
nnoremap <silent> <A-o> :FSHere<cr>
nnoremap <silent> <localleader>oh :FSSplitLeft<cr>
nnoremap <silent> <localleader>oj :FSSplitBelow<cr>
nnoremap <silent> <localleader>ok :FSSplitAbove<cr>
nnoremap <silent> <localleader>ol :FSSplitRight<cr>
set tags=./tags;
let g:gutentags_ctags_exclude_wildignore = 1
let g:gutentags_ctags_exclude = [
  \'node_modules', '_build', 'build', 'CMakeFiles', '.mypy_cache', 'venv',
  \'*.md', '*.tex', '*.css', '*.html', '*.json', '*.xml', '*.xmls', '*.ui']

nnoremap <silent> <A-6> :Vista!!<CR>

command! -nargs=+ Vfb call vimspector#AddFunctionBreakpoint(<f-args>)

nnoremap <localleader>gd :call vimspector#Launch()<cr>
nnoremap <localleader>gc :call vimspector#Continue()<cr>
nnoremap <localleader>gs :call vimspector#Stop()<cr>
nnoremap <localleader>gR :call vimspector#Restart()<cr>
nnoremap <localleader>gp :call vimspector#Pause()<cr>
nnoremap <localleader>gb :call vimspector#ToggleBreakpoint()<cr>
nnoremap <localleader>gB :call vimspector#ToggleConditionalBreakpoint()<cr>
nnoremap <localleader>gn :call vimspector#StepOver()<cr>
nnoremap <localleader>gi :call vimspector#StepInto()<cr>
nnoremap <localleader>go :call vimspector#StepOut()<cr>
nnoremap <localleader>gr :call vimspector#RunToCursor()<cr>

nnoremap   <silent>   <F12>   :FloatermToggle<CR>
let g:floaterm_keymap_toggle = '<F12>'

"autocmd VimEnter * NERDTree

nnoremap <C-Space> :FZF<CR>
