call plug#begin()


" File Management
"   Nerdtree
        Plug 'scrooloose/nerdtree'
        Plug 'Xuyuanp/nerdtree-git-plugin'
        Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
        Plug 'airblade/vim-gitgutter'
"   FZF
        Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
        Plug 'junegunn/fzf.vim'
"   Other
        Plug 'tpope/vim-fugitive'

" DEV
    " General
        Plug 'neovim/nvim-lspconfig'
        Plug 'sbdchd/neoformat'
        Plug 'scrooloose/nerdcommenter'
        Plug 'glepnir/lspsaga.nvim'
        Plug 'derekwyatt/vim-fswitch'
        Plug 'ludovicchabant/vim-gutentags'
        Plug 'liuchengxu/vista.vim'
        Plug 'voldikss/vim-floaterm'
    " Debug
        Plug 'puremourning/vimspector', {'do': 'python3 install_gadget.py --enable-vscode-cpptools'}

" Eye Candy
"   Status Line
        Plug 'vim-airline/vim-airline'
        Plug 'vim-airline/vim-airline-themes'
"   Animations
        Plug 'camspiers/animate.vim'
        Plug 'camspiers/lens.vim'
"   Other
        Plug 'wfxr/minimap.vim'
        Plug 'junegunn/goyo.vim'
        Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

        Plug 'ryanoasis/vim-devicons'

" Colors
    Plug 'arcticicestudio/nord-vim'

" Misc
    Plug 'hkupty/nvimux'
    Plug 'dstein64/vim-startuptime'

call plug#end()
