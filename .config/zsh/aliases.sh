type exa&> /dev/null && alias ls='exa'
type exa&> /dev/null || alias ls='ls --color=auto'

alias l='ls -lH'
alias la='ls -laF'
alias ll='ls -l'

alias grep='grep --color'

alias t='tail -f'

alias gs='git status'
alias ga='git add'
alias gp='git push'
alias gpo='git push origin'
alias gc='git commit'
alias gl='git log'

alias tmuxk='tmux kill-session -t'
alias tmuxa='tmux attach -t'
alias tmuxl='tmux list-sessions'

alias cp='cp -i'
alias df='df -h'

alias .2='cd ../..'
alias ...='cd ../../'
alias .3='cd ../../../'
alias ....='cd ../../../'
alias .4='cd ../../../../'
alias .....='cd ../../../..'
alias .5='cd ../../../../../'
alias ......='cd ../../../../..'
alias .6='cd ../../../../../../'

alias md=mkdir

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

alias v=nvim
alias s=sxiv

alias za=zathura

alias nb=newsboat
alias nm=neomutt

alias tm=tmux
alias cl=clear

alias m=make
alias mi=sudo make install
alias mc=make clean
alias mr=make clean && make

alias cm=cmake
alias cg=cmake -G Ninja -Bbuild
alias cmb=cmb --build

alias sysctl=systemctl --user
alias ssysctl=sudo systemctl

alias nf=neofetch
alias pf=pfetch

alias y=yay
alias ys="y -S"
alias yq="y -Q"
alias yqg="y -Q | grep"
alias yi="y -Qi"
alias yu="y -Syu"
alias yr="y -R"
