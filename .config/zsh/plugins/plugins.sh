#!/bin/zsh
source $ZDOTDIR/plugins/agnoster/agnoster.zsh-theme

source $ZDOTDIR/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source $ZDOTDIR/plugins/zsh-z/zsh-z.plugin.zsh

# colored pages
source $ZDOTDIR/plugins/colored-man-pages_mod/colored-man-pages_mod.plugin.zsh

# calc
source $ZDOTDIR/plugins/calc.plugin.zsh/calc.plugin.zsh

# git
source $ZDOTDIR/plugins/git

# tmux
#source $ZDOTDIR/plugins/tmux/tmux.plugin.zsh

RPROMPT='$(git_prompt_string)'

source $ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
